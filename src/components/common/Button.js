import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ children, onPress, text }) => (
  <TouchableOpacity onPress={ onPress } style={ styles.button }>
    <Text style={ styles.text }>
      { children }
    </Text>
  </TouchableOpacity>
);

export { Button };

const styles = {
  button: {
    alignSelf: 'stretch',
    backgroundColor: '#fff',
    borderColor: '#007aff',
    borderRadius: 5,
    borderWidth: 1,
    marginHorizontal: 5,
  },

  text: {
    alignSelf: 'center',
    color: '#007aff',
    fontSize: 16,
    fontWeight: '600',
    paddingVertical: 10,
  },
};
