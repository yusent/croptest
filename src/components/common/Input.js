import React from 'react';
import { Platform, Text, TextInput, View } from 'react-native';

const Input = ({ label, style, ...props }) => {
  let inputStyle;

  if (Array.isArray(style)) {
    inputStyle = [styles.input, ...style];
  } else {
    inputStyle = [styles.input, style];
  }

  return (
    <View style={ styles.wrapper }>
      <Text style={ styles.label }>{ label }</Text>

      <TextInput { ...props } style={ inputStyle } />
    </View>
  );
};

export { Input };

const styles = {
  input: {
    color: '#000',
    flex: 2,
    fontSize: 18,
    height: 40,
    paddingVertical: 5,
    width: 200,
    ...Platform.select({
      android: {
        lineHeight: 25,
      },

      ios: {
        lineHeight: 24,
      },
    }),
  },

  label: {
    flex: 1,
    fontSize: 18,
    paddingLeft: 20,
  },

  wrapper: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    height: 40,
  },
};
