import React from 'react';
import { Text, View } from 'react-native';

const Header = (props) => (
  <View style={ styles.view }>
    <Text style={ styles.text }>{ props.text }</Text>
  </View>
);

export { Header };

const styles = {
  text: {
    fontSize: 20,
  },

  view: {
    alignItems: 'center',
    backgroundColor: '#f8f8f8',
    elevation: 5,
    height: 60,
    justifyContent: 'center',
    position: 'relative',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
  },
};
