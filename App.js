/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Dimensions,
  Image,
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import { Button } from './src/components/common';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.vpWidth = Dimensions.get('window').width;
    this.state = {};
  }

  openPicker() {
    ImagePicker.openPicker({
      width: 3000,
      height: 1000,
      cropping: true
    }).then(image => {
      this.setState({ img: image.path });
      console.log(this.state);
      console.log(image);
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Button
          onPress={ this.openPicker.bind(this) }
        >
          Open picker
        </Button>
        <Image
          style={{
            height: this.vpWidth / 3,
            width: this.vpWidth,
          }}
          source={{ uri: this.state.img }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
